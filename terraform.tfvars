vpc_cidr_block      = "10.0.0.0/16"
subnet_cidr_block   = "10.0.0.0/20"
avail_zone          = "eu-west-1a"
env_prefix          = "dev"
my_ip               = ["81.2.174.80/32"]
instance_type       = "t2.micro"
public_key_location = "/var/jenkins_home/.ssh/id_rsa.pub"
private_key_location = ""