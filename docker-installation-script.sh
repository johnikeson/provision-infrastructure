#!/bin/bash

# Update package information and install prerequisites
sudo apt-get update
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

# Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Set up the Docker repository
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Update the package database with Docker packages from the newly added repo
sudo apt-get update

# Install Docker CE
sudo apt-get install -y docker-ce

# Verify Docker installation
sudo docker --version

# Start Docker service
sudo systemctl start docker

# Enable Docker to start on boot
sudo systemctl enable docker

# Log in to Docker using your credentials
echo "${docker_password}" | sudo docker login --username "${docker_username}" --password-stdin
